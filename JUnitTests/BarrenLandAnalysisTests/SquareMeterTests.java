//@Author David C. Nyquist
//@Date January 2017
package BarrenLandAnalysisTests;

import static org.junit.Assert.*;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import BarrenLandAnalysis.SquareMeter;

public class SquareMeterTests 
{
	SquareMeter controlSquareMeter = new SquareMeter(5, 39);
	
	@Test
	public void checkForAdjacencyTestNorth() 
	{
		SquareMeter controlNorth = new SquareMeter(5, 40);
		assertTrue(controlSquareMeter.checkForAdjacency(controlNorth));
	}
	@Test
	public void checkForAdjacencyTestSouth() 
	{
		SquareMeter controlSouth = new SquareMeter(5, 38);
		assertTrue(controlSquareMeter.checkForAdjacency(controlSouth));
	}
	@Test
	public void checkForAdjacencyTestEast() 
	{
		SquareMeter controlEast = new SquareMeter(6, 39);
		assertTrue(controlSquareMeter.checkForAdjacency(controlEast));
	}
	@Test
	public void checkForAdjacencyTestWest() 
	{
		SquareMeter controlWest = new SquareMeter(4, 39);
		assertTrue(controlSquareMeter.checkForAdjacency(controlWest));
	}
	
	@Test
	public void checkForAdjacencyTestNoteAdjacent() 
	{
		SquareMeter controlNotAdjacent = new SquareMeter(-3, -6);
		assertFalse(controlSquareMeter.checkForAdjacency(controlNotAdjacent));
	}

	@Test
	public void getAdjacentKeysTest() 
	{
		List<String> controlList = new ArrayList<String>();
		controlList.add("4 39");
		controlList.add("5 38");	
		controlList.add("6 39");
		controlList.add("5 40");
		
		assertEquals(controlList, controlSquareMeter.getAdjacentKeys());
	}
		
	@Test
	public void buildKeyTest() {
		assertEquals(SquareMeter.buildKey(-5, 20), "-5 20");
	}
}
