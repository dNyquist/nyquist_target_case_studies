//@Author David C. Nyquist
//@Date January 2017
package BarrenLandAnalysisTests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import BarrenLandAnalysis.Land;
import BarrenLandAnalysis.LandAnalyzer;

public class LandAnalyzerTests 
{

	@Test
	public void parseInputTestValid()	
	{
		List<Land> testResult = LandAnalyzer.parseInput("{\"48 192 351 207\", \"48 392 351 407\", \"120 52 135 547\", \"260 52 275 547\"}");
		List<Land> control = new ArrayList<Land>();
		Land landOne = new Land(48, 192, 351, 207);
		Land landTwo = new Land(48, 392, 351, 407);
		Land landThree = new Land(120, 52, 135, 547);
		Land landFour = new Land(260, 52, 275, 547);
		control.add(landOne);
		control.add(landTwo);
		control.add(landThree);
		control.add(landFour);
		assertTrue(testResult.equals(control));
	}
	
	@Test
	public void parseInputTestInvalid()	
	{
		List<Land> testResult = LandAnalyzer.parseInput("Invalid Input"); 
		assertEquals(testResult, null);
	}
	
	@Test
	public void parseOutputTest()
	{
		List<Integer> testList = new ArrayList<Integer>();
		testList.add(153123134);
		testList.add(9);
		testList.add(1);
		testList.add(20);
		testList.add(-16);
		testList.add(544);
		
		String control = "-16 1 9 20 544 153123134";
		assertEquals(control, LandAnalyzer.parseOutput(testList));
		
	}

}
