//@Author David C. Nyquist
//@Date January 2017
package BarrenLandAnalysisTests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.junit.Test;

import BarrenLandAnalysis.Land;
import BarrenLandAnalysis.LandAnalyzer;

public class LandTests 
{
	@Test
	public void pouplateLandMassTest()
	{
		Land testLand = new Land(0, 0, 2, 4);
		HashSet<String> expectedReuslt = new HashSet<String>();
		expectedReuslt.add("0 0");
		expectedReuslt.add("0 1");
		expectedReuslt.add("0 2");
		expectedReuslt.add("0 3");
		expectedReuslt.add("0 4");
		expectedReuslt.add("1 0");
		expectedReuslt.add("1 1");
		expectedReuslt.add("1 2");
		expectedReuslt.add("1 3");
		expectedReuslt.add("1 4");
		expectedReuslt.add("2 0");
		expectedReuslt.add("2 1");
		expectedReuslt.add("2 2");
		expectedReuslt.add("2 3");
		expectedReuslt.add("2 4");
		
		assertTrue(testLand.getLandMass().equals(expectedReuslt));
	}
	
	@Test
	public void calculateFertileLandMassesTestNoBarrenLand()
	{
		Land testLand = new Land(0, 0, 399, 599);
		List<Integer> control = new ArrayList<Integer>();
		control.add(240000);
		assertEquals(testLand.calculateFertileLandMasses(), control);
		
	}
	
	@Test
	public void calculateFertileLandMassesTestSomeBarrenLand()
	{
		List<Land> barrenLand = LandAnalyzer.parseInput("{\"48 192 351 207\", \"48 392 351 407\", \"120 52 135 547\", \"260 52 275 547\"}");
		Land testLand = new Land(barrenLand, 0, 0, 399, 599);
		List<Integer> control = new ArrayList<Integer>();
		control.add(22816);
		control.add(192608);
		assertEquals(testLand.calculateFertileLandMasses(), control);
		
	}
	
	@Test
	public void calculateFertileLandMassesTestAllBarrenLand()
	{
		List<Land> barrenLand = LandAnalyzer.parseInput("{\"0 0 399 599\"}");
		Land testLand = new Land(barrenLand, 0, 0, 399, 599);
		List<Integer> control = new ArrayList<Integer>();
		assertEquals(testLand.calculateFertileLandMasses(), control);
		
	}

	@Test
	public void combineAdjacentLandMassesTestSomeAdjacent() 
	{
		HashSet<String> setOne = new HashSet<String>();
		setOne.add("0 1");
		setOne.add("0 2");
		setOne.add("0 3");
		setOne.add("1 1");
		setOne.add("1 2");
		setOne.add("2 1");
		
		HashSet<String> setTwo = new HashSet<String>();
		setTwo.add("9 71");
		setTwo.add("8 71");
		setTwo.add("7 71");
		setTwo.add("7 72");
		
		HashSet<String> setThree = new HashSet<String>();		
		setThree.add("2 5");
		setThree.add("2 4");
		setThree.add("2 3");
		setThree.add("2 2");
		setThree.add("1 3");
		
		List<HashSet<String>> testList = new ArrayList<HashSet<String>>();
		testList.add(setOne);
		testList.add(setTwo);
		testList.add(setThree);
		List<Integer> result = Land.combineAdjacentLandMasses(testList);
		
		List<Integer> expectedResult = new ArrayList<Integer>();
		expectedResult.add(4);
		expectedResult.add(11);
		
		assertTrue(expectedResult.equals(result));
	}
	
	@Test
	public void combineAdjacentLandMassesTestAllAdjacent() 
	{
		HashSet<String> setOne = new HashSet<String>();
		setOne.add("0 1");
		setOne.add("0 2");
		setOne.add("0 3");
		setOne.add("1 1");
		setOne.add("1 2");
		setOne.add("2 1");
		
		HashSet<String> setTwo = new HashSet<String>();
		setTwo.add("1 3");
		setTwo.add("1 4");
		
		HashSet<String> setThree = new HashSet<String>();		
		setThree.add("2 5");
		setThree.add("2 4");
		setThree.add("2 3");
		setThree.add("2 2");
		
		List<HashSet<String>> testList = new ArrayList<HashSet<String>>();
		testList.add(setOne);
		testList.add(setTwo);
		testList.add(setThree);
		List<Integer> result = Land.combineAdjacentLandMasses(testList);
		
		List<Integer> expectedResult = new ArrayList<Integer>();
		expectedResult.add(12);
		
		assertTrue(expectedResult.equals(result));
	}
	
	@Test
	public void combineAdjacentLandMassesTestAllAdjacentNone() 
	{
		HashSet<String> setOne = new HashSet<String>();
		setOne.add("0 1");
		setOne.add("0 2");
		setOne.add("0 3");
		setOne.add("1 1");
		setOne.add("1 2");
		setOne.add("2 1");
		
		HashSet<String> setTwo = new HashSet<String>();
		setTwo.add("9 71");
		setTwo.add("8 71");
		setTwo.add("7 71");
		setTwo.add("7 72");
		
		HashSet<String> setThree = new HashSet<String>();		
		setThree.add("22 5");
		setThree.add("22 4");
		setThree.add("22 3");
		setThree.add("22 2");
		setThree.add("21 3");
		
		List<HashSet<String>> testList = new ArrayList<HashSet<String>>();
		testList.add(setOne);
		testList.add(setTwo);
		testList.add(setThree);
		List<Integer> result = Land.combineAdjacentLandMasses(testList);
		
		List<Integer> expectedResult = new ArrayList<Integer>();
		expectedResult.add(6);
		expectedResult.add(4);
		expectedResult.add(5);
		
		assertTrue(expectedResult.equals(result));
	}
	
	@Test
	public void combineAdjacentLandMassesTestEmpty() 
	{	
		List<HashSet<String>> testList = new ArrayList<HashSet<String>>();
		List<Integer> result = Land.combineAdjacentLandMasses(testList);		
		List<Integer> expectedResult = new ArrayList<Integer>();
		
		assertTrue(expectedResult.equals(result));
	}
}
