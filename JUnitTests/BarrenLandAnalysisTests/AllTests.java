package BarrenLandAnalysisTests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ LandAnalyzerTests.class, LandTests.class, SquareMeterTests.class })
public class AllTests {

}
