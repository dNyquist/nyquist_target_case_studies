# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Target Technical Assessment Case Studies for David C. Nyquist
* Version 1.0

### How do I get set up? ###

* How to run program: NyquistBarrenLandAnalysis\src\BarrenLandAnalysis\LandAnalyzer.java
* How to run tests: NyquistBarrenLandAnalysis\JUnitTests\BarrenLandAnalysisTests\AllTests.java
* Deployment instructions: 

### How do use this program? ###

* user input:

     Enter a list of the plots of barren land in the following format:

     {"xAxisMin yAxisMin xAxisMax yAxisMax", "xAxisMin yAxisMin xAxisMax yAxisMax", ...}

     Where xAxisMin, yAxisMin, xAxisMax, and yAxisMax are all integers.

     Example of valid input:

     {"48 192 351 207", "48 392 351 407", "120 52 135 547", "260 52 275 547"}

     Pleaes note:

          The minimum value for xAxisMin is 0.
          The minimum value for yAxisMin is 0.
          The maximum value for xAxisMax is 399.
          The maximum value for xAxisMax is 599.
          Any values entered beyond these limits will be disregarded by this program.
          If you wish to enter no plots of barren land simply enter nothing and click enter, this will run the scenario where no land is barren.

     To exit the program please input the the following value: EXIT

* output:

     The results will be output as a list of integers sorted into ascending order, where each integer represents the size, in square meters, of a continuous plot of fertile land.
     
     Example of valid output:

     22816 191776

* After receiving output you will be prompted again to enter input, allowing the user to easily run multiple cases. To exit the program please input the the following value: EXIT

### Who do I talk to? ###

* David C. Nyquist david.nyquist@alumni.augsburg.edu 763.913.6550