//@Author David C. Nyquist
//@Date January 2017
package BarrenLandAnalysis;
import java.util.ArrayList;
import java.util.List;

//one square meter of land
public class SquareMeter
{
	int xAxis; //the xAxis value of this SquareMeter
	int yAxis; //the yAxis value of this SquareMeter
		
	//constructs a new SquareMeter based of input xAxis and yAxis values
	public SquareMeter(int xAxis, int yAxis) 
	{
		this.xAxis = xAxis;
		this.yAxis = yAxis;
	}
		
	//constructs a new SquareMeter object from its key where
	//said key is a string in the format "x y" where x is the value of the SquareMeters xAxis
	//and 'y' is the value of the SquareMeters yAxis
	public SquareMeter(String key) 
	{
		String[] split= key.split(" ");
		this.xAxis = Integer.parseInt(split[0]);
		this.yAxis = Integer.parseInt(split[1]);
	}
		
	//returns true if the input SquareMeter is adjacent the the SquareMeter and returns false otherwise
	//Note: that adjacency is defined as being immediately to the north, south, east, or west
	//of a SquareMeter as such SquareMeters that are connected diagonally are not considered adjacent.
	//e.g. (2, 2) and (2, 3) are adjacent but (2, 2) and (3, 3) are not adjacent
	public boolean checkForAdjacency(SquareMeter squareMeter)
	{
		boolean nodesAreAdjacent = false;
		if (((this.xAxis - squareMeter.xAxis == 1 || this.xAxis - squareMeter.xAxis == -1) && this.yAxis == squareMeter.yAxis)
		    || ((this.yAxis - squareMeter.yAxis == 1 || this.yAxis - squareMeter.yAxis == -1) && this.xAxis == squareMeter.xAxis))
		{
			nodesAreAdjacent = true;
		}
		return nodesAreAdjacent;
	}
	
	//returns a List<String> where each value is the key for a SquareMeter
	//that would be geographically adjacent to this SquareMeter
	//Note: that adjacency is defined as being immediately to the north, south, east, or west
	//of a SquareMeter as such SquareMeters that are connected diagonally are not considered adjacent.
	//e.g. (2, 2) and (2, 3) are adjacent but (2, 2) and (3, 3) are not adjacent
	public List<String> getAdjacentKeys()
	{
		List<String> adjacentKeys = new ArrayList<String>();
		adjacentKeys.add(buildKey(xAxis - 1, yAxis));
		adjacentKeys.add(buildKey(xAxis, yAxis - 1));
		adjacentKeys.add(buildKey(xAxis + 1, yAxis));
		adjacentKeys.add(buildKey(xAxis, yAxis + 1));
		return adjacentKeys;
	}
		
	//creates a key for a SquareMeter object to be used in HashMap, HashSets, Lists, etc..
	//said key is a string in the format "x y" where x is the value of the SquareMeters xAxis
	//and 'y' is the value of the SquareMeters yAxis
	public static String buildKey(int xAxis, int yAxis)
	{
		String key = Integer.toString(xAxis) + " " + Integer.toString(yAxis);
		return key;
	}
}