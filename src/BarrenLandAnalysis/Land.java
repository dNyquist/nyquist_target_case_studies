//@Author David C. Nyquist
//@Date January 2017
package BarrenLandAnalysis;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

//a section of land whose outermost limits are defined
//by x axis and y axis minimums and maximums to form a rectangle
//within said outer limits can exist any shape of land
//the land is measured in complete square meters along the 
//x axis and y axis
public class Land 
{
	private HashSet<String> landMass; // a set of all the square meters of land that make up this parcel of land in the from "x y"
									  //where 'x' is the value of the x axis for that square meter 
									  //and 'y' is the value of the yAxis for that squareMeter
	private int xAxisMin; // the minimum xAxis of this parcel of land
	private int yAxisMin; // the minimum yAxis of this parcel of land
	private int xAxisMax; // the maximum xAxis of this parcel of land
	private int yAxisMax; // the maximum yAxis of this parcel of land
	
	
	//constructor to create a new rectangular Land object with all square meters of barren land removed from
	//the landMass class variable
	public Land(List<Land> barrenLand, int xAxisMin, int yAxisMin, int xAxisMax, int yAxisMax)
	{
		this.xAxisMin = xAxisMin;
		this.yAxisMin = yAxisMin;
		this.xAxisMax = xAxisMax;
		this.yAxisMax = yAxisMax;
		pouplateLandMass();
		for (int i = 0; i < barrenLand.size(); i++)
		{
			landMass.removeAll(barrenLand.get(i).landMass);
		}	
	}

	//constructor to create a new rectangular Land object
	public Land(int xAxisMin, int yAxisMin, int xAxisMax, int yAxisMax) 
	{
		this.xAxisMin = xAxisMin;
		this.yAxisMin = yAxisMin;
		this.xAxisMax = xAxisMax;
		this.yAxisMax = yAxisMax;
		pouplateLandMass();
	}
	
	
	// populates the landMass class variable with all possible values based on the various axis class Variables
	private void pouplateLandMass()
	{
		landMass = new HashSet<String>();
		for (int i = xAxisMin; i <= xAxisMax; i++)
		{
			for (int j = yAxisMin; j <= yAxisMax; j++)
			{
				landMass.add(Integer.toString(i) + " " + Integer.toString(j));
			}
		}
	}
	
	//determines the number and size of contiguous land masses that exist in the landMass class variable
	//and returns that information in the form of a List<Integer> where each Integer in the list represents
	//the size of one of the aforementioned contiguous land masses.
	public List<Integer> calculateFertileLandMasses()
	{
		//converting the landMass HashSet into an ordered list
		//greatly improves runtime of this method
		List<String> land = new ArrayList<String>(landMass);
		Collections.sort(land); 
		
		List<HashSet<String>> fertileLandMasses = new ArrayList<HashSet<String>>();
		Map<String, Integer> placementMap = new HashMap<String, Integer>();	
		
		//iterates through each square meter of land and places it in
		for (int i = 0; i < land.size(); i++)
		{
			SquareMeter squareMeter = new SquareMeter(land.get(i));
			List<String> keys = squareMeter.getAdjacentKeys();
			boolean nodePlaced = false; //true if a match for current value was found false otherwise
			//iterates through each option value that could be considered 
			//adjacent to the current square meter trying to be placed
			innerLoop:	
			for (int j = 0; j < keys.size(); j++)
			{
				//the case where a value adjacent to the current value has already been placed
				//in the fertileLandMasses method variable. In this case the current value
				//is placed in the same HashSet as the adjacent value found.
				if(placementMap.containsKey(keys.get(j)))
				{
					fertileLandMasses.get(placementMap.get(keys.get(j))).add(land.get(i));
					placementMap.put(land.get(i), placementMap.get(keys.get(j)));
					nodePlaced = true;
					break innerLoop;
				}
			}
			//the case where no values adjacent to the current value have been placed so far
			//in which case the current value is added to a new HashSet which is then added to 
			//the fretileLandMasses method variable
			if (!nodePlaced)
			{
				HashSet<String> newHashSet = new HashSet<String>();
				newHashSet.add(land.get(i));
				fertileLandMasses.add(newHashSet);
				placementMap.put(land.get(i), fertileLandMasses.size() - 1);
			}
		}
		//combine any of the HashSets contained in the fertileLandMasses method variable
		//and return a List<Integer> where each Integer represents the size() of one of the
		//final combined HashSets
		return combineAdjacentLandMasses(fertileLandMasses);

	}
	
	//takes a List<HashSet<String>> as input, where each String represents one square meter of land.
	//Strings are to be in the following format: "(x y)" where 'x' is the integer value of the axis for that
	//square meter of land and 'y' is the integer value of the yAxis for that square meter of land. Each
	//Hash Set represents a portion of continuous square meters of land; which is to say that all the square meters
	//denoted by that HashSet are immediately adjacent to at least one other square meter entered in that particular HashSet
	//this method will check to see if any of the separate HashSets entered could actually be connected to form one continuous
	//parcel of land and combine any and all such HashSets that can be so connected. It will then return a List<Integer> 
	//wherein each integer represents the size in square meters of the final condensed list of adjacent land.
	public static List<Integer> combineAdjacentLandMasses(List<HashSet<String>> input)
	{
		List<Integer> output = new ArrayList<Integer>();
		while (!input.isEmpty())
		{
			HashSet<String> hashSet = input.get(0);
			input.remove(0);
			boolean matched = false;
			Iterator<String> iterator = hashSet.iterator();
			whileLoop:
			while(iterator.hasNext())
			{
				List<String> keys = new SquareMeter(iterator.next()).getAdjacentKeys();
				for (int i = 0; i < input.size(); i++)
				{
					for (int j = 0; j < keys.size(); j++)
					{
						if(input.get(i).contains(keys.get(j)))
						{
							input.get(i).addAll(hashSet);
							matched = true;
							break whileLoop;
						}
					}
				}
			}
			if (!matched)
			{
				output.add(hashSet.size());
			}
		}
		return output;
	}

	//currently only used for the purposes of JUnit testing
	public HashSet<String> getLandMass() {
		return landMass;
	}
	
	//equals method currently only used for JUnit testing
	@Override
	public boolean equals(Object obj)
	{
		boolean output = false;
		try
		{
			Land land = (Land)obj;
			if (this.xAxisMax == land.xAxisMax && this.xAxisMin == land.xAxisMin &&
				this.yAxisMax == land.yAxisMax && this.yAxisMin == land.yAxisMin &&
				this.landMass.equals(land.landMass))
			{
				output = true;
			}
		}
		catch (Exception e)
		{
			
		}	
		return output;
	}
}