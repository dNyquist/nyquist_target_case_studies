//@Author David C. Nyquist
//@Date January 2017
package BarrenLandAnalysis;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


//takes in user input concerning sections of barren land
//and returns of all the fertile land remaining within
//the rectangle formed by the coordinates (0, 0) (399, 599)
//the returned data twill be a list of all the sizes 
//of contiguous fertile land sorted into ascending order
public class LandAnalyzer 
{
	//A introductory message that is displayed to the user giving them instructions on how to use this program.
	public static final String INTRO_MESSAGE = "Please enter a list of the plots of barren land in the following format:\n\n"
											 + "{\"xAxisMin yAxisMin xAxisMax yAxisMax\", \"xAxisMin yAxisMin xAxisMax yAxisMax\", ...}\n\n"
											 + "    - Where xAxisMin, yAxisMin, xAxisMax, and yAxisMax are all integers.\n\n"
											 + "Example of valid input:\n\n\n"
											 + "{\"48 192 351 207\", \"48 392 351 407\", \"120 52 135 547\", \"260 52 275 547\"}\n\n\n"
											 + "Pleaes note:\n\n"
											 + "    - The minimum value for xAxisMin is 0.\n"
											 + "    - The minimum value for yAxisMin is 0.\n"
											 + "    - The maximum value for xAxisMax is 399.\n"
											 + "    - The maximum value for xAxisMax is 599.\n"
											 + "    - Any values entered beyond these limits will be disregarded by this program.\n"
											 + "    - If you wish to enter no plots of barren land simply enter nothing and click enter, this will run the scenario where no land is barren.\n\n"
											 + "To exit the program please input the the following value: EXIT";
						
	
	//parses input into a List<Land> so that operations may be performed on it by the program.
	//will return null if the user input is invalid
	public static List<Land> parseInput(String input)
	{
		List<Land> barrenLands;
		//the case where the user has provided no input
		if (input == null || input.isEmpty())
		{
			barrenLands = new ArrayList<Land>();
		}
		//the case where the user has provided some input
		else
		{
			//attempt to parse the user input
			try
			{
				barrenLands = new ArrayList<Land>();
				input = input.replace("{", "");
				input = input.replace("}", "");
				input = input.replace("\"", "");
				input = input.replace(", ", ",");
				String[] barrenLandInput = input.split(",");
				for (int i = 0; i < barrenLandInput.length; i++)
				{
					String[] plotPoints = barrenLandInput[i].split(" ");
					barrenLands.add(new Land(Integer.parseInt(plotPoints[0]), Integer.parseInt(plotPoints[1]), Integer.parseInt(plotPoints[2]), Integer.parseInt(plotPoints[3])));         
				}
			}
			//if input could not be parsed return null to denote that user input was invalid
			catch (Exception e)
			{
				barrenLands = null;
			}
		}
		
		
		return barrenLands;
	}
	
	//parses a List<Integer> into this programs valid output format
	//where each Integer represents the size of one of the contiguous land masses 
	//contained in the land structure input to this program 
	public static String parseOutput(List<Integer> input)
	{
		String output = "";
		Collections.sort(input);
		for (int i = 0; i < input.size(); i ++)
		{
			output += input.get(i) + " ";
		}
		return output.substring(0, output.length() - 1);
	}
	
	//runs the program printing instructions to the user
	//continuing to take in and process input on new formations of land
	//returning the results until the user tells the program to terminate
	public static void main(String[] args) throws IOException
	{
		String input; //holds user input
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in)); //reads user input
		boolean continueProgram = true; //true if the program should continue to rerun false if the program should terminate
		//continues to rerun the program allowing the user to check as many input cases as they like until
		//the user enters 'EXIT' terminating the program
		while(continueProgram)
		{
			System.out.println(INTRO_MESSAGE); //prints INTRO_MESSAGE for the user
			input = bufferedReader.readLine(); //read user input
			//the case where the user as requested to end the program
			if (input.equals("EXIT"))
			{
				continueProgram = false;
			}
			else
			{
				List<Land> barrenLands = parseInput(input); //parse user input
				//the case where the user provided valid input
				if (barrenLands != null)
				{
					Land farmLand = new Land (barrenLands, 0, 0, 399, 599);
					System.out.println(parseOutput(farmLand.calculateFertileLandMasses())); //process user input and print result to user
					System.out.println("\n\n\n\n");
				}
				//the case where the users input was invalid.
				else
				{
					System.out.println("Invalid Input entered.\n\n"); //notify user that there input was invalid.
				}
			}
		}	
	}
}
